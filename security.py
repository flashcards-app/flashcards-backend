from functools import wraps
import jwt
from flask import request, jsonify
from config import app, db
from model import UserModel
from werkzeug.security import generate_password_hash, check_password_hash
from datetime import datetime, timedelta


def token_required(func):
    @wraps(func)
    def decorated(*args, **kwargs):
        token = request.headers['x-access-token'] if 'x-access-token' in request.headers else None
        if not token:
            return jsonify({'message': 'Token not defined'}), 401
        try:
            data = jwt.decode(token, app.config['JWT_KEY'], algorithms=['HS256'])
            user_model = UserModel.query.filter_by(username=data['username']).first()
        except:
            return jsonify({'message': 'Token is invalid'}), 401
        return func(user_model, *args, **kwargs)
    return decorated


@app.route('/login', methods=['POST'])
def login():
    auth = request.authorization
    if not auth or not auth.username or not auth.password:
        return {'message': 'Login details are incomplete'}, 401
    user_model = UserModel.query.filter_by(username=auth.username).first()
    if not user_model:
        return {'message': 'User not found'}, 401
    if check_password_hash(user_model.password_hash, auth.password):
        token = jwt.encode({
            'username': user_model.username,
            'exp': datetime.utcnow() + timedelta(minutes=30)
        }, app.config['JWT_KEY'], algorithm='HS256')
        return jsonify({'token': token})
    return {'message': 'Incorrect password'}


@app.route('/register', methods=['POST'])
def register():
    user_data = request.get_json()
    if 'username' not in user_data or 'password' not in user_data:
        return jsonify({'message': 'Register details are incomplete'}), 400
    user_model = UserModel.query.filter_by(username=user_data['username']).first()
    if user_model:
        return jsonify({'message': 'User with the username exists'})
    password_hash = generate_password_hash(user_data['password'], method='sha256')
    user_model = UserModel(username=user_data['username'], password_hash=password_hash)
    db.session.add(user_model)
    db.session.commit()
    return jsonify({'message': 'New user created'}), 201


@app.route('/security_test', methods=['GET'])
@token_required
def security_test(user_model):
    return jsonify({'message': 'Security Test'})
