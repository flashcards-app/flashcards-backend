from config import app
from security import *
from resources import *

if __name__ == "__main__":
    app.run(debug=True)
