from flask_restful import Resource
from flask import jsonify
from config import api
from security import token_required


class Word(Resource):
    @token_required
    def get(self, user_model):
        return jsonify({'message': 'Get Word'})

    def post(self):
        return jsonify({'message': 'Post Word'})

    def put(self):
        return jsonify({'message': 'Put Word'})

    def delete(self):
        return jsonify({'message': 'Delete Word'})


class Phrase(Resource):
    def get(self):
        return jsonify({'message': 'Get Phrase'})

    def post(self):
        return jsonify({'message': 'Post Phrase'})

    def put(self):
        return jsonify({'message': 'Put Phrase'})

    def delete(self):
        return jsonify({'message': 'Delete Phrase'})


class Lang(Resource):
    def get(self):
        return jsonify({'message': 'Get Lang'})

    def post(self):
        return jsonify({'message': 'Post Lang'})

    def put(self):
        return jsonify({'message': 'Put Lang'})

    def delete(self):
        return jsonify({'message': 'Delete Lang'})


class Level(Resource):
    def get(self):
        return jsonify({'message': 'Get Level'})

    def post(self):
        return jsonify({'message': 'Post Level'})

    def put(self):
        return jsonify({'message': 'Put Level'})

    def delete(self):
        return jsonify({'message': 'Delete Level'})


class Category(Resource):
    def get(self):
        return jsonify({'message': 'Get Category'})

    def post(self):
        return jsonify({'message': 'Post Category'})

    def put(self):
        return jsonify({'message': 'Put Category'})

    def delete(self):
        return jsonify({'message': 'Delete Category'})


class Flashcard(Resource):
    def get(self):
        return jsonify({'message': 'Get Flashcard'})

    def post(self):
        return jsonify({'message': 'Post Flashcard'})

    def put(self):
        return jsonify({'message': 'Put Flashcard'})

    def delete(self):
        return jsonify({'message': 'Delete Flashcard'})


class User(Resource):
    def get(self):
        return jsonify({'message': 'Get User'})

    def post(self):
        return jsonify({'message': 'Post User'})

    def put(self):
        return jsonify({'message': 'Put User'})

    def delete(self):
        return jsonify({'message': 'Delete User'})


class UserFlashcard(Resource):
    def get(self):
        return jsonify({'message': 'Get User Flashcard'})

    def post(self):
        return jsonify({'message': 'Post User Flashcard'})

    def put(self):
        return jsonify({'message': 'Put User Flashcard'})

    def delete(self):
        return jsonify({'message': 'Delete User Flashcard'})


api.add_resource(Word, '/word')
api.add_resource(Phrase, '/phrase')
api.add_resource(Lang, '/lang')
api.add_resource(Level, '/level')
api.add_resource(Category, '/category')
api.add_resource(Flashcard, '/flashcard')
api.add_resource(User, '/user')
api.add_resource(UserFlashcard, '/user-flashcard')
