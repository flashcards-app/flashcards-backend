from model import db

### DB creating ###
db.create_all()
### CMD checking ###
# sqlite3 flashcards.db - Run sqlite3 for flashcards.db.
# sqlite> .table - List all tables.
# sqlite> pragma table_info(table_name) - List all columns for table_name.
