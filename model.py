from config import db


class WordModel(db.Model):
    __tablename__ = 'word'
    id = db.Column(db.Integer, primary_key=True)
    word = db.Column(db.String)
    lang_id = db.Column(db.Integer, db.ForeignKey('lang.id'))
    flashcard_id = db.Column(db.Integer, db.ForeignKey('flashcard.id'))


class PhraseModel(db.Model):
    __tablename__ = 'phrase'
    id = db.Column(db.Integer, primary_key=True)
    phrase = db.Column(db.String)
    lang_id = db.Column(db.Integer, db.ForeignKey('lang.id'))
    flashcard_id = db.Column(db.Integer, db.ForeignKey('flashcard.id'))
    # Used to be distinguished and correctly matched to the second lang if more than one
    flashcard_phrase_id = db.Column(db.Integer)


class LangModel(db.Model):
    __tablename__ = 'lang'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    user_flashcard = db.relationship('UserFlashcardModel')


class LevelModel(db.Model):
    __tablename__ = 'level'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    flashcard = db.relationship('FlashcardModel')


class CategoryModel(db.Model):
    __tablename__ = 'category'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    flashcard = db.relationship('FlashcardModel')


class FlashcardModel(db.Model):
    __tablename__ = 'flashcard'
    id = db.Column(db.Integer, primary_key=True)
    level_id = db.Column(db.Integer, db.ForeignKey('level.id'))
    category_id = db.Column(db.Integer, db.ForeignKey('category.id'))
    word = db.relationship('WordModel')
    phrase = db.relationship('PhraseModel')
    user_flashcard = db.relationship('UserFlashcardModel')


class UserModel(db.Model):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String)
    password_hash = db.Column(db.String)
    # In the future:
    # email = db.Column(db.String)
    user_flashcard = db.relationship('UserFlashcardModel')


class UserFlashcardModel(db.Model):
    __tablename__ = 'user_flashcard'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    flashcard_id = db.Column(db.Integer, db.ForeignKey('flashcard.id'))
    lang_id = db.Column(db.Integer, db.ForeignKey('lang.id'))
    all_attempts = db.Column(db.String)
    correct_attempts = db.Column(db.String)
